# 03 Basic React Components - I

In previous lessons we have done installation of react native toolsets and made ourself familiar with react native project structure. Now its time to get our hands dirty and actually work on some code!

## Target application sketch
Wait! Before diving into code, let´s have a quick look at the wireframes of our target that we will be building - starting from this lession and will continue in future lessons.

As you can see, we will start with an empty projectm then design a reusable card, and work on its layout, then we will link data to it and add a search bar, then we will add support for multiple pages in the app and last but not the least we will cover support for multiple languages!

![](app-wireframes.png)

Now, lets start creating a new project. Note that we will then delete generated UI components so that we can start from scratch!


## Creating an empty project

Create an empty project

```
npx react-native init <project_name>​
```

Then, test that app is running fine on Android or iOS depending on your setup!​

```
# start server
npm react-native start

# start app on android
npx react-native run-android 
# start app on ios
npx react-native run-ios
```

Now, let´s delete the contents of App.tsx file​, which is the only UI component generated. We will keep other configuration and boiler plate files.

Note that if your run your app with empty App.tsx file, it will fail. It expects valid contents in the applications entry point i.e App.tsx file. Lets fix this next!!​


## Hello world screen
Now let´s write a simple hello-worls screen by adding following contents to the ```Àpp.tsx``` file.

<img align="right" src="helloworld-screen.png" alt="drawing" width="150"/>

```
import React from 'react';
import { Text, View } from 'react-native';

const Inft2508App = () => {
  return (
    <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
      <Text>
        Hello! 🎉
      </Text>
    </View>
  );
}

export default Inft2508App;

```

<br clear="both"/>

As you can notice, the above ```App.tsx``` file has 3 parts​ - import statements on the top, functional component, and then an export statement.

The functional component returns a <View> with <Text> element inside it.​ Let´s ignore "style" property for the time being​, we come to it later!

Note also that when application renders, it internally does many React.createElement() function calls. Therefore we need to import "React" element, eventhoough it seems to be not used in our code!​

Now, if you run the app it should look like the following! 
The ```Hello!``` text is displayed centered to the screen due to the ```style``` property of the ```View``` element!


## Styling react native components
In the previous section we have implemented a simple home screen with a text "Hello!" displayed at center!

Now, instead of just text, lets try to wrap it into another (inner) view element with some heignt, width, backgdound color and rounded corner. The idea is to make it a kind of card component!


So, this would be the first try! 

<img align="right" src="fixed-size-innerview.png" alt="drawing" width="250"/>

```
import React from 'react';
import { Text, View } from 'react-native';

const Inft2508App = () => {
  return (
    <View style={{ 
        flex: 1, 
        justifyContent: "center", 
        alignItems: "center" }}>
      <View style={{ 
          backgroundColor: "#9FE8FF",
          height: 100,
          width: 150,
          borderRadius: 10
        }}>
        <Text>
          Hello! 🎉
        </Text>
      </View>
    </View>
  );
}

export default Inft2508App;
```

<br clear="both"/>

As you can see, a view with blueish background and with height 100 and width 150 is centered. But the text 'Hello!' is not quite where we wanted!

In order to achieve that, we need to understand concept of layout - i.e. how components are in a screen with respect to each other and in available space. One of the mostly used layout algorithm in react native is ```flexbox```. So in the next section we will look into that.



## flexbox layout algorithm
Readers are encouraged to refer react native official documentation on flexbox here - https://reactnative.dev/docs/flexbox

As mentioned before, flexbox is one of the mostly used layout algorithm when it comes to layout in react-native apps​! It provides several properties which can be used to customize components layout and properties are be applied as ```style``` in a component. For example
- flex
- flexDirection
- justifyContent
- alignItems
- alignSelf
- flexWrap
- alignContent
- +++

 Often, flexbox uses a combination of ```flexDirection```, ```alignItems```, and ```justifyContent``` properties to achieve the right layout!​

Ok, now we know something about about flexbox. And if you look back our original example, we are actually already using this algotirhm - in the outermost view! For example like this

    flex: 1, 
    justifyContent: "center", 
    alignItems: "center" 

Then, let´s try to understand what does these properties mean?

### 'flex' and 'flexDirection' properties
The ```flex``` property defines how you are going to fill the component in the available space​. As you can see in the figure below, the enclosing outermost view has two inner views and they are filled with the ratio of 1:2 according to the value of their 'flex' style property!​ You can try it yourself how it works here - https://reactnative.dev/docs/flexbox#flex

![](flex-prop.png)

The ```flexDirection``` property controls the direction in which the children of a node are laid out i.e. the main axis! The possible values are ´row', 'column'(default), 'row-reverse', and 'column-reverse'​. In the figure above, the enclosing view has two child nodes/views and they are laid out one after another in a column, as per the value of 'flexDirection' property in their parent. Try yourself here for more examples of 'flexDIrection' here https://reactnative.dev/docs/flexbox#flex-direction


### 'justifyContent' property - the main axis
The 'justifyContent' property describes how to align children within the main-axis of their container. ​Possible values are 'flex-start' | 'flex-end' | 'center' | 'space-between' | 'space-around' | 'space-evenly'​. Try yourself here https://reactnative.dev/docs/flexbox#justify-content​

The figure below shows six different ways of placing elements along the main axis 
- flex-start (default value)
- flex-end
- center
- space-between
- space-around
- space-evenly


![](justify-content.png)


### 'alignItems' property - the cross axis
The 'alignItems' property describes how to align children within the cross-axis of their container. ​Possible values are 'flex-start' | 'flex-end' | 'center' | 'stretch' (default) | 'baseline'​. Try yourself here https://reactnative.dev/docs/flexbox#align-items​

The figure below shows diffetent ways of placing elements along the cross-axis.
- stretch (default value)
- flex-start
- flex-end
- center
- baseline

![](align-items.png)

### 'alignSelf' property
It has the same options and effect as ```alignItems``` but instead of affecting the children within a container, you can apply this property to a single child to change its alignment within its parent. ​It overrides any option set by the parent with ```alignItems```.​


## Lets get back to our exampel and see flexbox in action
We have started to wrap 'Hello!' text within a view like this and we have seen that the text actually appears towards top-left corner.


<img align="right" src="fixed-size-innerview.png" alt="drawing" width="250"/>

```
import React from 'react';
import { Text, View } from 'react-native';

const Inft2508App = () => {
  return (
    <View style={{ 
        flex: 1, 
        justifyContent: "center", 
        alignItems: "center" }}>
      <View style={{ 
          backgroundColor: "#9FE8FF",
          height: 100,
          width: 150,
          borderRadius: 10
        }}>
        <Text>
          Hello! 🎉
        </Text>
      </View>
    </View>
  );
}

export default Inft2508App;
```

<br clear="both"/>


Now it should be clear how inner view gets displayed at the center of the screen! Well, this is because of outer view´s ```justifyContent``` and ```alignItems``` properties i.e childs are centered on both main axis and cross axis. 





Note that in our example above, the ```flex: 1``` in the outermost view represents that it will cover all the available space i.e. the whole screen as it is the root element!

Note also that when the main axis (flexDirection) is not given, the default value is ```column```. For the sake of experiment, you can change it to ```row```, add one more 'View' element inside the outermost view and see what happens.


<img align="right" src="flexDir-row.png" alt="drawing" width="250"/>

```
import React from 'react';
import { Text, View } from 'react-native';

const Inft2508App = () => {
  return (
    <View style={{ 
        flex: 1, 
        justifyContent: "center", 
        alignItems: "center",
        flexDirection: "row" }}>
      <View style={{ 
          backgroundColor: "#9FE8FF",
          height: 100,
          width: 150,
          borderRadius: 10
        }}>
        <Text>Hello!</Text>
      </View>
      <View style={{ 
          backgroundColor: "#9FE8FF",
          height: 100,
          width: 150,
          borderRadius: 10
        }}>
        <Text> Hello!</Text>
      </View>
    </View>
  );
}

export default Inft2508App;
```

<br clear="both"/>

Now, lets add two more view elements inside the outer most container view and inspect what happens!
It actually looks ugly now. Elements are rendered out of the displable screen. 

<img align="right" src="flexDir-row-many.png" alt="drawing" width="250"/>

```
import React from 'react';
import { Text, View } from 'react-native';

const Inft2508App = () => {
  return (
    <View style={{ 
        flex: 1, 
        justifyContent: "center", 
        alignItems: "center",
        flexDirection: "row" }}>
      <View style={{ 
          backgroundColor: "#9FE8FF",
          height: 100,
          width: 150,
          borderRadius: 10
        }}>
        <Text>Hello!</Text>
      </View>
      <View style={{ 
          backgroundColor: "#9FE8FF",
          height: 100,
          width: 150,
          borderRadius: 10
        }}>
        <Text> Hello!</Text>
      </View>
      <View style={{ 
          backgroundColor: "#9FE8FF",
          height: 100,
          width: 150,
          borderRadius: 10
        }}>
        <Text> Hello!</Text>
      </View>
      <View style={{ 
          backgroundColor: "#9FE8FF",
          height: 100,
          width: 150,
          borderRadius: 10
        }}>
        <Text> Hello!</Text>
      </View>
    </View>
  );
}

export default Inft2508App;
```
<br clear="both"/>

One solution to fix this is by using ```flexWrap: wrap``` property to the outermost container view element!


<img align="right" src="flexWrap.png" alt="drawing" width="250"/>

```
import React from 'react';
import { Text, View } from 'react-native';

const Inft2508App = () => {
  return (
    <View style={{ 
        flex: 1, 
        justifyContent: "center", 
        alignItems: "center",
        flexDirection: "row",
        flexWrap: "wrap"}}>
      <View style={{ 
          backgroundColor: "#9FE8FF",
          height: 100,
          width: 150,
          borderRadius: 10
        }}>
        <Text>Hello!</Text>
      </View>
      <View style={{ 
          backgroundColor: "#9FE8FF",
          height: 100,
          width: 150,
          borderRadius: 10
        }}>
        <Text> Hello!</Text>
      </View>
      <View style={{ 
          backgroundColor: "#9FE8FF",
          height: 100,
          width: 150,
          borderRadius: 10
        }}>
        <Text> Hello!</Text>
      </View>
      <View style={{ 
          backgroundColor: "#9FE8FF",
          height: 100,
          width: 150,
          borderRadius: 10
        }}>
        <Text> Hello!</Text>
      </View>
    </View>
  );
}

export default Inft2508App;
```
<br clear="both"/>


You will see that lauout is a bit better now, but the boxes (inner view elements) are all aligned top! But why?? This is because of the usage of ```flexWrap``` property. With ```flexWrap``` one should use ```alignContent``` property that defines the distribution of lines along the cross-axis. This only has effect when items are wrapped to multiple lines using flexWrap. So, lets use ```alignContent``` and inspect!

<img align="right" src="flexWrap-alignContent.png" alt="drawing" width="250"/>

```
import React from 'react';
import { Text, View } from 'react-native';

const Inft2508App = () => {
  return (
    <View style={{ 
        flex: 1, 
        justifyContent: "center", 
        alignItems: "center",
        flexDirection: "row",
        flexWrap: "wrap",
        alignContent: "center"}}>
      <View style={{ 
          backgroundColor: "#9FE8FF",
          height: 100,
          width: 150,
          borderRadius: 10
        }}>
        <Text>Hello!</Text>
      </View>
      <View style={{ 
          backgroundColor: "#9FE8FF",
          height: 100,
          width: 150,
          borderRadius: 10
        }}>
        <Text> Hello!</Text>
      </View>
      <View style={{ 
          backgroundColor: "#9FE8FF",
          height: 100,
          width: 150,
          borderRadius: 10
        }}>
        <Text> Hello!</Text>
      </View>
      <View style={{ 
          backgroundColor: "#9FE8FF",
          height: 100,
          width: 150,
          borderRadius: 10
        }}>
        <Text> Hello!</Text>
      </View>
    </View>
  );
}

export default Inft2508App;
```
<br clear="both"/>

## Finally,
To wrap this lesson, let´s again go back to the simpler version of our app and try to implement the following things
- Horizontally divide inner view into two parts – one for image and another for text​
- Center both image and text components along both main and cross axises

Note that 
- First we have imported 'Image' component from react-native package and added 'Image' element inside inner view element, besides 'Text' element. That means inner view has now two child elements!
- Then we have added flexbox properties to the inner view such that main axis is column and childern are centered along both main and cross axises and that 
- Then for convinience, both 'Text' and 'Image' elements are wrapped around 'View' such that ```justifyContent``` for 'Image' wrapper view is set "flex-end", and ``justifyContent``` for 'Text' wrapper view is set "flex-start"!

The outcome looks like the following.

<img align="right" src="card-centered.png" alt="drawing" width="250"/>

```
import React from 'react';
import { Text, View, Image } from 'react-native';

const Inft2508App = () => {
  return (
    <View style={{ 
        flex: 1, 
        justifyContent: "center", 
        alignItems: "center"}}>
      <View style={{ 
          backgroundColor: "#9FE8FF",
          height: 100,
          width: 150,
          borderRadius: 10,
          justifyContent: "center", 
          alignItems: "center",
        }}>
        <View style= {{flex: 1, justifyContent: "flex-end"}}>
          <Image
            style= {{width: 20, height: 20}}
            source={require("./logo.png")}
          />
        </View>
        <View style= {{flex: 1, justifyContent: "flex-start"}}>
          <Text>Hello!</Text>
        </View>
      </View>
    </View>
  );
}

export default Inft2508App;
```
<br clear="both"/>


Congratulations, you have so far learned usage of basic react native components including their styling and layouts. In the next lesson we will continue on this, and have more focus on writing better/reusabe components and handling application data using states!


# Exercise #2

<img align="right" src="exercise-2.png" alt="drawing" width="250"/>

Assume that you have been asked to design a mobile app for lotto (original website: https://www.norsk-tipping.no/lotteri/lotto/)!

You ask in this exercise is to design a landing page, which will look something like the figure below. Note that you can just focus on components and their layout and ignore other features like clicking, data handing and so on (they will come in the following exercises)!

<br clear="both"/>
