
import React from 'react';
import { Text, View } from 'react-native';

const Inft2508App = () => {
  return (
    <View style={{ flex: 1, flexDirection: "row", flexWrap: "wrap", justifyContent: "center", alignItems: "center" }}>
      <View style={{ 
          backgroundColor: "#9FE8FF",
          height: 100,
          width: 150,
          borderRadius: 10
        }}>
        <Text>
          Hello! 🎉
        </Text>
      </View>
      <View style={{ 
          backgroundColor: "#50FF96",
          height: 100,
          width: 150,
          borderRadius: 10
        }}>
        <Text>
          Hello! 🎉
        </Text>
      </View>
      <View style={{ 
          backgroundColor: "#9FE8FF",
          height: 100,
          width: 150,
          borderRadius: 10
        }}>
        <Text>
          Hello! 🎉
        </Text>
      </View>
      <View style={{ 
          backgroundColor: "#9FE8FF",
          height: 100,
          width: 150,
          borderRadius: 10
        }}>
        <Text>
          Hello! 🎉
        </Text>
      </View>
    </View>
  );
}

export default Inft2508App;